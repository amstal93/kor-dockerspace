mkdir -p /home/ubuntu/.ssh
cp /share/xenial-petalinux-dotssh/* /home/ubuntu/.ssh
chown -R ubuntu.ubuntu /home/ubuntu/.ssh
chmod 600 /home/ubuntu/.ssh/*

/etc/init.d/tftpd-hpa start
/usr/sbin/sshd -D
