################################################################################
# Summary:
# Docker container suitable for software development. Can be hosted
# from docker for windows. XWindows forwarding tested and working with
# putty and VcXsrv 1.20.1.4 on windows 10 x86_64.
#
# Notes:
# The startup.sh script expects /share/cosmic-sshd-dotssh/ to contain at
# least an authorized_keys file. All files in this directory will be copied 
# upon startup to the ubuntu users '.ssh' directory before starting the ssh
# daemon.
#
# /share is intended to be a shared volume with the docker host
# /persist is intended to be a volume used to store persistant data
#
# Example usage (or use docker-compose and deploy within a swarm):
#
# mkdir -p d:/share/cosmic-sshd-dotssh
# copy id_rsa.pub d:/share/cosmic-sshd-dotssh/authorized_keys
#
# docker run -d \
#     	-v d:/share:/share \
#     	-v petalinux-dev:/persist \
#     	-p 22:22 \
#	cosmic-sshd:15
#
# ssh ubuntu@localhost:22 # or use putty, etc.
#
# Example backup procedure (will show progress bar):
#
# docker run -it \
#	-v d:/share:/share \
#	-v android-dev:/persist \
# 	-v c:/path/to/dir/containing_backup_script:/mnt \
#	cosmic-sshd:25 /bin/bash /mnt/backup_persist.sh
#

FROM ubuntu:18.10

LABEL maintainer="Adam Scislowicz <adam.scislowicz@gmail.com>"


# Handle all COPYs early so builds fail early if not properly staged.
COPY startup.sh /etc


VOLUME /share
VOLUME /persist


RUN chmod 755 /etc/startup.sh
RUN yes | unminimize
RUN dpkg --add-architecture i386
RUN apt-get -y update
RUN apt-get -y dist-upgrade
RUN apt-get -y install tzdata apt-utils

RUN apt-get \
        -o Acquire::Queue-Mode=access \
        -y install \
	vim-nox cscope build-essential gdb lsof dstat strace \
	pkg-config screen openssh-server openssh-client traceroute \
	iputils-ping bash-completion zlib1g-dev libpython-dev \
        bison flex autoconf texinfo unzip help2man gawk libtool-bin \
        libncurses-dev bc iproute2 psmisc openssl sudo pv pbzip2 \
        nmap tcpdump netcat git tig \
        manpages manpages-dev manpages-posix manpages-posix-dev man-db \
	libgtk-3-dev python-pygraphviz libsqlite3-dev \
        libxml2-dev libgsl-dev libboost-all-dev python-goocalendar \
        python-cairo-dev

# setup git-lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash

RUN apt-get -y install libpoco-dev libgtest-dev libgmock-dev \
	libjemalloc-dev mysql-client libnl-route-3-dev astyle \
        qemu-system-arm redis googletest googletest-tools \
        openjdk-8-jdk \
	golang \
        qtcreator mesa-utils libgl1-mesa-glx qt5-default \
        cmake ninja-build libedit-dev swig doxygen xz-utils \
 	git-lfs \
	net-tools diffstat chrpath socat gcc-multilib \
        libsdl1.2-dev zlib1g-dev:i386 locales \
        rsync xterm cpio curl lsb-core libssl-dev \
	libncurses5-dev libncurses5 \
        libncursesw5 libncursesw5-dev

RUN echo "export GOROOT=/usr/lib/go-1.10" > /etc/profile.d/golang.sh
RUN echo "export PATH=$PATH:$GOROOT/bin" >> /etc/profile.d/golang.sh
RUN locale-gen en_US en_US.UTF-8 && update-locale LC_ALL="en_US.UTF-8" LANG="en_US.UTF-8"

RUN mkdir /var/run/sshd

# Setup the ubuntu user & group.

RUN groupadd ubuntu -g 1000
RUN useradd -rms /bin/bash -d /home/ubuntu -g ubuntu -G sudo -u 1000 -p "$(openssl passwd -1 ubuntu)" ubuntu
RUN chown -R ubuntu.ubuntu /persist /share

USER ubuntu
WORKDIR /home/ubuntu
RUN echo "shell bash" > /home/ubuntu/.screenrc
RUN echo "startup_message off" >> /home/ubuntu/.screenrc

# /persist hooks
RUN echo "if [ -f /persist/.bashrc ]; then . /persist/.bashrc; fi" >> /home/ubuntu/.bashrc
RUN ln -s /persist/.vimrc /home/ubuntu/.vimrc
RUN ln -s /persist/.config /home/ubuntu/.config
RUN ln -s /persist/.gitconfig /home/ubuntu/.gitconfig
RUN ln -s /persist/third_party/google/android/.android /home/ubuntu/.android

USER root
RUN echo "X11UseLocalhost no" >> /etc/ssh/sshd_config
RUN echo "ubuntu ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ubuntu_nopasswd

# Cleanup
RUN rm -rf /var/lib/apt/lists/* /tmp/*

EXPOSE 22
CMD  ["/bin/bash", "/etc/startup.sh"]
