# rebuild the docker image(s) in the compose file
docker-compose build

# deploy the stack onto your local swarm
docker stack deploy --compose-file .\docker-compose.yml devenv

# multi-compose-file deploy
docker stack deploy --compose-file .\docker-compose.yml \
  --compose-file .\docker-compose-local.yml devenv

# look around
docker stack services devenv
docker network ls
docker stack ls
docker service ls

# remove the whole stack
docker stack rm devenv

# if you want to bring just one host in the stack down and then
# back up, say to pickup a rebuilt image you can use the
# following procedure:
docker service scale devenv_cosmic-sshd=0 # bring down to zero instances
docker service scale devenv_cosmic-sshd=1 # bring back to one instance


https://docs.docker.com/engine/swarm/stack-deploy/

