#!/bin/bash

timestr=`date +"%Y%m%d-%H.%M.%S-%Z"`
fname="persist-$timestr.tar.bz2"

cd /
echo "Determining size of persist directory..."
SIZE=`du -sb persist | cut -f 1`
echo "${SIZE} bytes. Archiving to /share/$fname:"
tar pcf - persist | \
    pv -s ${SIZE} | pbzip2 -c > /share/$fname
sha512sum /share/$fname >> /share/persist.sha512s
echo "Done."
exit 0
